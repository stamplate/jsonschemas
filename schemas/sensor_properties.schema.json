{
  "$schema": "http://json-schema.org/draft-06/schema#",
  "title": "SensorProperties",
  "description": "JSON object of the `properties` property of an OGC STA `Sensor` entity",
  "type": "object",
  "properties": {
    "@context": {
      "type": "object",
      "properties": {
        "@version": {
          "type": "number",
          "const": 1.1
        },
        "@import": {
          "type": "string"
        },
        "@vocab": {
          "type": "string",
          "const": "http://schema.org/"
        }
      },
      "required": [
        "@version",
        "@import",
        "@vocab"
      ]
    },
    "jsonld.id": {
      "description": "link to a view page of the respective entity in the source system",
      "$ref": "#/$defs/url"
    },
    "jsonld.type": {
      "type": "string",
      "const": "SensorProperties"
    },
    "identifier": {
      "description": "link to a persistent identifier for the sensor",
      "$ref": "#/$defs/optionalUrl"
    },
    "isVariantOf": {
      "description": "device type of this sensor, e.g., thermometer",
      "type": "object",
      "properties": {
        "jsonld.type": {
          "type": "string",
          "const": "ProductGroup"
        },
        "name": {
          "description": "device-type name",
          "type": "string"
        },
        "definition": {
          "$ref": "#/$defs/optionalUrl"
        }
      },
      "required": [
        "jsonld.type"
      ]
    },
    "isVirtual": {
      "description": "flag indicating whether the sensor is a physical device (false) or, e.g., a computation procedure",
      "type": "boolean"
    },
    "model": {
      "type": "string"
    },
    "manufacturer": {
      "$ref": "#/$defs/organization"
    },
    "serialNumber": {
      "type": "string"
    },
    "images": {
      "description": "array of images of the sensor",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "@type": {
            "type": "string",
            "const": "ImageObject"
          },
          "caption": {
            "type": "string"
          },
          "contentUrl": {
            "$ref": "#/$defs/url"
          }
        },
        "required": [
          "@type"
        ]
      }
    },
    "responsiblePersons": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "jsonld.type": {
            "type": "string",
            "const": "Role"
          },
          "roleName": {
            "description": "name of the role the person has w.r.t. the thing, e.g., Owner",
            "type": "string"
          },
          "definition": {
            "$ref": "#/$defs/optionalUrl"
          },
          "responsiblePersons": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "jsonld.id": {
                  "$ref": "#/$defs/url"
                },
                "jsonld.type": {
                  "type": "string",
                  "const": "Person"
                },
                "givenName": {
                  "type": "string"
                },
                "familyName": {
                  "type": "string"
                },
                "email": {
                  "type": "string"
                },
                "affiliation": {
                  "$ref": "#/$defs/organization"
                },
                "identifier": {
                  "description": "link to a persistent identifier for the person, e.g., the ORCID",
                  "$ref": "#/$defs/optionalUrl"
                }
              },
              "required": [
                "jsonld.type"
              ]
            }
          }
        },
        "required": [
          "jsonld.type"
        ]
      }
    }
  },
  "required": [
    "@context",
    "jsonld.id",
    "jsonld.type"
  ],
  "$defs": {
    "url": {
      "type": "string",
      "pattern": "^https*://"
    },
    "optionalUrl": {
      "type": "string",
      "pattern": "^$|^https*://"
    },
    "organization": {
      "type": "object",
      "properties": {
        "jsonld.type": {
          "type": "string",
          "const": "Organization"
        },
        "name": {
          "type": "string"
        },
        "identifier": {
          "description": "link to a persistent identifier for the organization, e.g., the ROR",
          "$ref": "#/$defs/optionalUrl"
        }
      },
      "required": [
        "jsonld.type"
      ]
    }
  }
}
